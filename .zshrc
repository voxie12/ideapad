export TERM="xterm-256color"
export EDITOR=nvim
export VISUEL=nvim
export PATH=~/.local/bin:$PATH

# fetch
#paleofetch
#fetchit
pfetch

autoload -U colors && colors
autoload -Uz promptinit && promptinit
setopt auto_cd
#prompt redhat
# history
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.cache/zsh/history
mkdir -p ~/.cache/zsh && touch "$HISTFILE"

# alias
alias nf='neofetch'
alias v='nvim'
alias vim='nvim'
alias sudo='doas --'
alias y='paru -Syu --noconfirm'
alias szsh='source ~/.zshrc'
alias off='sudo poweroff'
alias reboot='sudo reboot'
alias smci='doas make clean install'
alias ls='exa'
alias ll='exa -la'
alias mirror='doas reflector --verbose -l 10 -c GB --sort rate --save /etc/pacman.d/mirrorlist'
alias xw='xwallpaper --zoom '
alias r='ranger'
alias q='nvim ~/.config/qtile/config.py'

# yadm
alias ys='yadm status'
alias ya='yadm add -u :/ && yadm status'
function yc {
  echo "Please enter your commit:"
  read commit
  yadm commit -m "$commit"
  yadm status
}
alias yp='yadm push && yadm status'

# completions
autoload -Uz compinit && compinit
zstyle ':completion:*' menu select
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
_comp_options+=(globdots)

# like vim bindkey
KEYTIMEOUT=1
bindkey -v
bindkey '^E' end-of-line
bindkey '^A' beginning-of-line
bindkey '^R' history-incremental-pattern-search-backward

# source
source /usr/share/zsh/plugins/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh 2>/dev/null
source /usr/share/zsh/plugins/zsh-fzf-plugin/fzf.plugin.zsh 2>/dev/null

# prompt
eval "$(starship init zsh)"
