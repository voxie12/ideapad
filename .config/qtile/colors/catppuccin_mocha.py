colors = [
 ["#f5e0dc", "#f5e0dc"],  # 0 rosewater
 ["#f2cdcd", "#f2cdcd"],  # 1 flamingo
 ["#f5c2e7", "#f5c2e7"],  # 2 pink
 ["#cba6f7", "#cba6f7"],  # 3 Mauve
 ["#f38ba8", "#f38ba8"],  # 4 red
 ["#eba0ac", "#eba0ac"],  # 5 Maroon
 ["#fab387", "#fab387"],  # 6 peach
 ["#f9e2af", "#f9e2af"],  # 7 yellow
 ["#a6e3a1", "#a6e3a1"],  # 8 green
 ["#94e2d5", "#94e2d5"],  # 9 teal
 ["#89dceb", "#89dceb"],  # 10 sky
 ["#74c7ec", "#74c7ec"],  # 11 sapphire
 ["#89b4fa", "#89b4fa"],  # 12 blue
 ["#b4befe", "#b4befe"],  # 13 lavender
 ["#CDD6F4", "#CDD6F4"],  # 14 text
 ["#bac2de", "#bac2de"],  # 15 subtext1
 ["#a6adc8", "#a6adc8"],  # 16 subtext0
 ["#9399b2", "#9399b2"],  # 17 Overlay2
 ["#7f849c", "#7f849c"],  # 18 Overlay1
 ["#6c7086", "#6c7086"],  # 19 Overlay0
 ["#585b70", "#585b70"],  # 20 surface2
 ["#45475a", "#45475a"],  # 21 surface1
 ["#313244", "#313244"],  # 22 surface0
 ["#1E1E2E", "#1E1E2E"],  # 23 base
 ["#181825", "#181825"],  # 24 mantle
 ["#11111b", "#11111b"],  # 25 curst
]
