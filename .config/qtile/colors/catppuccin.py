colors = [
 ["#1E1E2E", "#1E1E2E"],  # 0 black
 ["#f38ba8", "#f38ba8"],  # 1 red
 ["#a6e3a1", "#a6e3a1"],  # 2 green
 ["#f9e2af", "#f9e2af"],  # 3 yellow
 ["#89b4fa", "#89b4fa"],  # 4 blue
 ["#f5c2e7", "#f5c2e7"],  # 5 pink
 ["#94e2d5", "#94e2d5"],  # 6 teal
 ["#b4befe", "#b4befe"],  # 7 lavender
 ["#cba6f7", "#cba6f7"],  # 8 Mauve
 ["#74c7ec", "#74c7ec"],  # 9 sapphire
 ["#bac2de", "#bac2de"],  # 10 white
 ["#CDD6F4", "#CDD6F4"],   # 11 subtext 1
 ["#fab387", "#fab387"],   # 12 peach
 ["#f5e0dc", "#f5e0dc"],   # 13 rosewater
 ["#f2cdcd", "#f2cdcd"],   # 14 flamingo
]
