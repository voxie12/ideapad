import os
import re
import socket
import subprocess
from libqtile import qtile
from libqtile.config import Click, Drag, Group, KeyChord, Key, Match, Screen
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook, extension
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from typing import List  # noqa: F401
from colors.catppuccin_mocha import colors


mod = "mod4"
terminal = guess_terminal("alacritty")
browser = "firefox"
#prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())

keys = [
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    #Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),
    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key(
        [mod, "shift"],
        "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    #Key([mod], "space", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),

    # dmenu
    Key([mod], 'space', lazy.run_extension(extension.DmenuRun(
    #dmenu_command="dmenu_run",
    dmenu_prompt="λ",
    dmenu_font="Hack Nerd Font",
    background="#1e1e2e",
    foreground="#cba6f7",
    selected_background="#cba6f7",
    selected_foreground="#1e1e2e",
    #dmenu_lines=42,
    dmenu_bottom=False
    #dmenu_height=24,  # Only supported by some dmenu forks
    ))),

# custom
    Key([], "XF86AudioRaiseVolume", lazy.spawn("pamixer -i 5"), desc='Volume Up'),
    Key([], "XF86AudioLowerVolume", lazy.spawn("pamixer -d 5"), desc='volume down'),
    Key([], "XF86AudioMute", lazy.spawn("pamxier -t"), desc='Volume Mute'),
    Key([], "XF86AudioPlay", lazy.spawn("playerctl play-pause"), desc='playerctl'),
    Key([], "XF86AudioPrev", lazy.spawn("playerctl previous"), desc='playerctl'),
    Key([], "XF86AudioNext", lazy.spawn("playerctl next"), desc='playerctl'),
    Key([], "XF86MonBrightnessUp", lazy.spawn("xbacklight -inc 10%"), desc='brightness UP'),
    Key([], "XF86MonBrightnessDown", lazy.spawn("xbacklight -dec 10%"), desc='brightness Down'),
    Key([mod],"e", lazy.spawn("thunar"), desc='file manager'),
    Key([mod],"w", lazy.spawn(browser), desc='launches my browser'),
    #Key([mod], "h", lazy.spawn("roficlip"), desc='clipboard'),
    #Key([mod], "s", lazy.spawn("flameshot gui"), desc='Screenshot'),
]

    

#groups = [Group(i) for i in "123456789"]
groups = [
    Group(
        '1',
        label="一",
        matches=[
            Match(wm_class=["Alacritty", "kitty"]),
            ],
        layout="monadtall"
    ),
    Group('2', label="二", layout="max", matches=[Match(wm_class=["firefox", "brave"])]),
    Group('3', label="三", layout="monadtall", matches=[Match(wm_class=["telegram-desktop"])]),
    Group('4', label="四", layout="monadtall", matches=[Match(wm_class=["qBittorrent"])]),
    Group('5', label="五", layout="monadtall"), 
    Group('6', label="六", layout="monadtall"), 
    Group('7', label="七", layout="monadtall"), 
    Group('8', label="八", layout="monadtall"), 
    Group('9', label="九", layout="monadtall"), 
    Group('0', label="十", layout="max", matches=[Match(wm_class=["mpv"])])
]

for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # mod1 + shift + letter of group = switch to & move focused window to group
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(i.name),
            ),
            # Or, use below if you prefer not to switch to that group.
            # # mod1 + shift + letter of group = move focused window to group
            #Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
            #    desc="move focused window to group {}".format(i.name)),
        ]
    )
#groups = [Group("DEV", layout='monadtall'),
#          Group("WWW", layout='monadtall'),
#          Group("SYS", layout='monadtall'),
#          Group("SYS", layout='monadtall'),
#          Group("DOC", layout='monadtall'),
#          Group("VBOX", layout='monadtall'),
#          Group("CHAT", layout='monadtall'),
#          Group("MUS", layout='monadtall'),
#          Group("VID", layout='monadtall'),
#          Group("GFX", layout='floating')]

# Allow MODKEY+[0 through 9] to bind to groups, see https://docs.qtile.org/en/stable/manual/config/groups.html
# MOD4 + index Number : Switch to Group[index]
# MOD4 + shift + index Number : Send active window to another Group
from libqtile.dgroups import simple_key_binder
dgroups_key_binder = simple_key_binder("mod4")

layout_theme = {"border_width": 2,
                "margin": 5,
                "border_focus": colors[13],
                "border_normal": colors[19]
                }

layouts = [
    #layout.Columns(border_focus_stack=["#d75f5f", "#8f3d3d"], border_width=4),
    layout.Max(**layout_theme),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    layout.MonadTall(**layout_theme),
    layout.MonadWide(**layout_theme),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
    #layout.floating.Floating(),
]

widget_defaults = dict(
        #font="Hack Nerd Font"
    font="Hack Nerd Font",
    fontsize=10,
    padding=5,
    foreground = colors[14],
    background = colors[23]
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.GroupBox(
                    highlight_method="text", 
                    active = colors[13],
                    inactive = colors[19],
                    rounded = False,
                    disable_drag= True,
                    highlight_color = "#ff0000",
                    this_current_screen_border = colors[13],
                    this_screen_border = colors[13],
                    other_current_screen_border = colors[19],
                    other_screen_border = colors[19],
                    foreground = colors[13],
                    background = colors[23],
                    ),
                #widget.CurrentLayout(),
                widget.Prompt(),
                #widget.WindowName(),
                widget.Spacer(
                    length = bar.STRETCH
                    ),
                widget.Chord(
                    chords_colors={
                        "launch": ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                #widget.TextBox("default config", name="default"),
                #widget.TextBox("Press &lt;M-r&gt; to spawn", foreground="#d75f5f"),
                #widget.OpenWeather(
                #    app_key="6d7a8d682008c2f1e5d72c12953bc020",
                #    location='London',
                #    format="{location_city}: {icon} {main_temp}°{units_temperature}",
                #    ),
                widget.CheckUpdates(
                    distro = "Arch_checkupdates",
                    update_interval = 1800,
                    display_format = "Updates: {updates} ",
                    foreground = colors[5],
                    background = colors[23],
                    colors_have_updates = colors[5],
                    colors_no_updates = colors[5],
                    mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(terminal + '-e sudo pacman -Syu')},
                    ),
                widget.Wttr(
                    location={'London': 'home'},
                    format = '%C, %t'
                    ),
                widget.Sep(),
                #widget.Net(prefix='M'),
                #widget.Sep(),
                #widget.Volume(),
                #widget.Sep(),
                #widget.Memory(
                #    fmt = "Mem: {}",
                #    ),
                #widget.Sep(),
                #widget.ThermalSensor(
                #    threshold = 40,
                #    fmt = "  Temp: {}",
                #        ),
                #widget.Sep(),
                # NB Systray is incompatible with Wayland, consider using StatusNotifier instead
                # widget.StatusNotifier(),
                #widget.Systray(),
                widget.BatteryIcon(
                    theme_path='~/.config/qtile/assets/Battery/',
                    scale=1,
                        ),
                widget.Battery(
                    #charge_char=" ",
                    #discharge_char="batt:",
                    #full_char=" ",
                    #font="Hack Nerd Font",
                    format="{percent:2.0%}",
                    ),
                widget.Sep(),
                widget.Clock(
                    format="%d/%m/%y - %R",
                    ),
                #widget.QuickExit(),
            ],
            25, # bar height
            #border_color=["0", "0", "0", "0"],
            #border_width=[0, 0, 0, 0],
            #margin = [15,60,6,60],
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.

wmname = "LG3D"

@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.run([home])
