#!/usr/bin/env bash 

xset r rate 250 50 &
xset s off -dpms &
setxkbmap gb &
xbacklight -set 40 &
unclutter --timeout 3 &
udiskie &

# wallpaper
xwallpaper --zoom ~/pic/wall/wallhaven-vqd9qm.jpg

# programs
picom -f &
lxsession &
killall redshift
#redshift -l $(curl -s "https://location.services.mozilla.com/v1/geolocate?key=geoclue" | jq -r '"\(.location.lat):\(.location.lng)"') &
redshift &
dunst &
