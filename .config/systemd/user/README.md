hi 
before u use it, please install geoclue first

```bash
sudo pacman -S geoclue --needed
```

* remember to do it WITHOUT sudo
```bash
systemctl --user enable --now geoclue-agent.service
```
